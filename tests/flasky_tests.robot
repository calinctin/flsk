*** Settings ***

Documentation    Various tests for flasky demo_app.
...              Run GUI tests:
...              robot --pythonpath pages:resources --loglevel TRACE -i gui -d "output/result-run-from-date-`date -R`" tests/flasky_tests.robot

Library     SeleniumLibrary
Library     PageObjectLibrary
Variables   variables.py

*** Test Cases ***

Register Random User And Validate
    [Setup]         Open Maximized
    [Teardown]      Close All Browsers
    [Tags]  gui
    Randomize Username
    Go To Page  RegistrationPage
    Register User   ${USERNAME}
    The Current Page Should Be      LoginPage
    Login  ${USERNAME}     ${DEFAULT_PASSWORD}
    The Current Page Should Be      UserInformationPage
    User Information Should Match   ${USERNAME}

Register Random User With NoDefault Password And Validate
    [Setup]         Open Maximized
    [Teardown]      Close All Browsers
    [Tags]  gui
    Randomize Username
    Go To Page  RegistrationPage
    Register User   ${USERNAME}    password10+
    The Current Page Should Be      LoginPage
    Login  ${USERNAME}     password10+
    The Current Page Should Be      UserInformationPage
    User Information Should Match   ${USERNAME}

Register Random User With NoDefault Password, NoDefault First Name And Validate
    [Setup]         Open Maximized
    [Teardown]      Close All Browsers
    [Tags]  gui
    Randomize Username
    Go To Page  RegistrationPage
    Register User   ${USERNAME}    _@password10+   Calin
    The Current Page Should Be      LoginPage
    Login  ${USERNAME}     _@password10+
    The Current Page Should Be      UserInformationPage
    User Information Should Match   ${USERNAME} Calin

Register Random User With NoDefault Password, NoDefault First Name, NoDefault Last Name , NoDefault Phone Number And Validate
    [Setup]         Open Maximized
    [Teardown]      Close All Browsers
    [Tags]  gui
    Randomize Username
    Go To Page  RegistrationPage
    Register User   ${USERNAME}    _@!@#$%^&*()_+password13213123120+   Calin   Constantin   654798616546
    The Current Page Should Be      LoginPage
    Login  ${USERNAME}     _@!@#$%^&*()_+password13213123120+
    The Current Page Should Be      UserInformationPage
    User Information Should Match   ${USERNAME} Calin   Constantin   654798616546

Register Random User With NoDefault Password, NoDefault First Name, NoDefault Last Name And Validate
    [Setup]         Open Maximized
    [Teardown]      Close All Browsers
    [Tags]  gui
    Randomize Username
    Go To Page  RegistrationPage
    Register User   ${USERNAME}    _@password13213123120+   Calin   Constantin
    The Current Page Should Be      LoginPage
    Login  ${USERNAME}     _@password13213123120+
    The Current Page Should Be      UserInformationPage
    User Information Should Match   ${USERNAME} Calin   Constantin

Login With Invalid Credentials
    [Setup]         Open Maximized
    [Teardown]      Close All Browsers
    [Tags]  gui
    Go To Page  LoginPage
    Login  invalid_user_name     _@password13213123120+wrong
    The Current Page Should Be      LoginFailurePage

*** Keywords ***

Open Maximized
    Open Browser   ${ROOT}     ${BROWSER}
    Maximize Browser Window

Randomize Username
    ${now}=     Get Time    format=epoch
    Set Suite Variable   ${USERNAME}    user${now}
