# flsk

Environment used:
Beside the app 
 - Mac OS with python 3.7 installed.
 - Pycharm 

Pycharm needs to use the dependencies found under requirements.txt.

Also there is a need to setup an venv that should look like this:

![Interpreter ](interpreter.png)

In order to be able to run the tests you need perform the following commands first( command line) :
 - Activate venv: ```source venv/bin/activate``` from root directory 
 of the project
 - Export this variable: ```export FLASK_APP=demo_app``` 
 - Initialize the data base: ```flask init-db```
 - Start the app: ```flask run --host=0.0.0.0 --port=8080```
 
 Output should look something like this:
 ```MachineIsMac:flsk calin$ ls
README.md		demo_app		pages			requirements.txt	run.sh			tests
MachineIsMac:flsk calin$ source venv/bin/activate
(venv) MachineIsMac:flsk calin$ export FLASK_APP=demo_app
(venv) MachineIsMac:flsk calin$ flask init-db
Initialized the database.
(venv) MachineIsMac:flsk calin$ flask run --host=0.0.0.0 --port=8080
 * Serving Flask app "demo_app"
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:8080/ (Press CTRL+C to quit)
  ```

Note: Be careful that no firewall or antivirus is preventing the app to run properly.
Test that the app runs by open this link http://0.0.0.0:8080/ from a browser
Should look like this:

![App](app.png)

Run the tests:

 - Tests are run using Chrome browser therefore there is a need to have also chrome driver install ( and added to out path)
Chrome driver can be downloaded from https://chromedriver.chromium.org/ 

In this example this version has been used: ``` Current stable release: ChromeDriver 80.0.3987.106 ```

 - GUI tests can be run from command prompt:
``` robot --pythonpath pages:resources --loglevel TRACE -i gui -d "output/result-run-from-date-`date -R`" tests/flasky_tests.robot ```

 - Navigate to ```output``` directory to check the test results.
