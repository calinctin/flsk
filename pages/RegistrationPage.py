from PageObjectLibrary import PageObject
from variables import DEFAULT_PASSWORD, DEFAULT_FIRST_NAME, \
    DEFAULT_LAST_NAME, DEFAULT_PHONE_NUMBER


class RegistrationPage(PageObject):
    PAGE_TITLE = "Register - Demo App"
    PAGE_URL = "/register"

    _locators = {
        "username": "id=username",
        "password": "id=password",
        "first_name": "id=firstname",
        "last_name": "id=lastname",
        "phone_number": "id=phone",
        "register": "Register"
    }

    def register_user(self, username, password=DEFAULT_PASSWORD, first_name=DEFAULT_FIRST_NAME,
                      last_name=DEFAULT_LAST_NAME, phone_number=DEFAULT_PHONE_NUMBER):
        """Register the user using stored details."""
        self.selib.input_text(self.locator.username, username)
        self.selib.input_text(self.locator.password, password)
        self.selib.input_text(self.locator.first_name, first_name)
        self.selib.input_text(self.locator.last_name, last_name)
        self.selib.input_text(self.locator.phone_number, phone_number)

        with self._wait_for_page_refresh():
            self.click_register_button()

    def click_register_button(self):
        self.selib.click_button(self.locator.register)
