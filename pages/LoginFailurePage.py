from PageObjectLibrary import PageObject


class LoginFailurePage(PageObject):
    PAGE_TITLE = "Login Failure - Demo App"
    PAGE_URL = "/error"