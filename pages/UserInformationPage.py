from PageObjectLibrary import PageObject
from variables import DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_PHONE_NUMBER


class UserInformationPage(PageObject):
    PAGE_TITLE = "User Information - Demo App"
    PAGE_URL = "/user"

    _locators = {
        "details_table": "id=content"
    }

    def user_information_should_match(self, username, first_name=DEFAULT_FIRST_NAME,
                                      last_name=DEFAULT_LAST_NAME, phone_number=DEFAULT_PHONE_NUMBER):
        """Validates used details"""
        expected_details = {
            "Username": username,
            "First name": first_name,
            "Last name": last_name,
            "Phone number": phone_number
        }
        actual_details = {}
        for row in range(2, 6):
            key = self.selib.get_table_cell(self.locator.details_table, row, 1)
            value = self.selib.get_table_cell(self.locator.details_table, row, 2)
            actual_details[key] = value
        self.logger.info(f"Expected user details: {expected_details}")
        self.logger.info(f"Actual user details: {actual_details}")
        differences = {k: expected_details[k] for k in set(expected_details) - set(actual_details)}
        if differences:
            raise ValueError(f"Details mismatch: {differences}")
