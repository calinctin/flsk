from PageObjectLibrary import PageObject


class LoginPage(PageObject):
    PAGE_TITLE = "Log In - Demo App"
    PAGE_URL = "/login"

    _locators = {
        "username": "id=username",
        "password": "id=password",
        "login": "Log In",
    }

    def login(self, username, password):
        """Login with specified used and password"""
        self.selib.input_text(self.locator.username, username)
        self.selib.input_password(self.locator.password, password)
        with self._wait_for_page_refresh():
            self.click_login()

    def click_login(self):
        """Click login button"""
        self.selib.click_button(self.locator.login)
